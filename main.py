import unpushed_layout
import pandas as pd
from export_staffs import get_staffs_group
import datetime
import make_shot
import data_clean
import os,sys
import argparse
from decouple import config


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-u', '--username', action='store', required=False, help='LDAP username')
    parser.add_argument('-p', '--password', action='store', required=False, help='LDAP password')

    args = parser.parse_args()
    today = datetime.datetime.today().date()
    pr_clean = data_clean.pr_clean()
    sub_clean = data_clean.sub_clean()
    pr_daily_total_detail = data_clean.get_pr_daily_total_detail(pr_clean)
    sub_daily_total_detail = data_clean.get_sub_daily_total_detail(sub_clean)
    pr_unpushed_detail_data = data_clean.get_pr_unpushed_data(pr_clean)
    sub_unpushed_detail_data = data_clean.get_sub_unpushed_data(sub_clean)
    """生成文件目录"""
    data_dir = os.path.join(os.environ.get('TEMP'), config('PROJECT_NAME'),
                            datetime.datetime.strftime(datetime.datetime.today(), '%Y%m%d'))

    if os.path.exists(data_dir) == False:
        os.makedirs(data_dir)

    """生成page_html """
    inputs = {'pr_daily_total_detail': pr_daily_total_detail,
              'sub_daily_total_detail': sub_daily_total_detail,
              'pr_unpushed_detail_data': pr_unpushed_detail_data,
              'sub_unpushed_detail_data': sub_unpushed_detail_data
              }

    html_name = config('HTML_NAME')
    html_path = os.path.join(data_dir, html_name)
    page_html = unpushed_layout.page_layout(inputs, html_path)

    """截取page图片"""
    image_name = 'image.png'
    image_filename = make_shot.webshot(html_path, data_dir, image_name)


    """获取SE邮件信息"""
    pr_sourcing = pr_unpushed_detail_data['Sourcing'].drop_duplicates()
    sub_sourcing = sub_unpushed_detail_data['Sourcing'].drop_duplicates()
    sourcing = pd.concat([pr_sourcing, sub_sourcing], axis=0)
    staff_info = get_staffs_group(args.username or config('LDAP_USER'), args.password or config(
        'LDAP_PASS')).staffs_data
    staff_info = staff_info[['Name_cn', 'Email']]
    recivers_se = pd.merge(sourcing, staff_info, left_on='Sourcing', right_on='Name_cn', how='inner')
    se_list = list(recivers_se['Sourcing'])
    recivers_email = list(recivers_se['Email'])

    """获取部门和每个se的excel未下推明细表"""
    pr_clean_data = pr_clean[['单据编号', '审核/建议日期','pr审核日期', '用途', '物料编码', '物料名称', '规格型号', '申请单位',
                              '批准数量','申请数量', '到货日期', '提前期', '建议采购日期', '采购员_head', '采购组_head', 'Sourcing',
                              '需求来源', '需求单据编号', 'PortalTitle', '研发项目', '需求人', '明细主键','终止日期', '业务关闭',
                              '终止日期', '数据状态', '订单关联数量', '关闭状态']]

    pr_unpushed_today_data = pr_unpushed_detail_data[['超期状态','超期天数', '明细主键']]\
                                                    [pr_unpushed_detail_data['统计日期'] == today]
    pr_unpushed_today_detail_data = pd.merge(pr_clean_data, pr_unpushed_today_data,
                                             left_on='明细主键',
                                             right_on='明细主键',
                                             how='inner')
    pr_unpushed_today_detail_data = pr_unpushed_today_detail_data[
                               ['Sourcing','超期天数','超期状态','单据编号', '审核/建议日期', 'pr审核日期', '建议采购日期','用途', '物料编码',
                                '物料名称','规格型号', '申请单位','批准数量','申请数量', '到货日期', '提前期',  '采购员_head',
                                '采购组_head', '需求来源', '需求单据编号', 'PortalTitle', '研发项目', '需求人',
                                '终止日期', '数据状态', '订单关联数量', '关闭状态','明细主键']].sort_values(by='超期天数',ascending=False)



    sub_clean_data = sub_clean[['单据编号', '单据类型', 'sub审核/计划开工日期','sub审核日期', '用途', '物料编码', '物料名称',
                                '规格型号', '单位', '数量','业务状态', '计划开工时间', '计划完工时间', '需求单据', '采购员_head',
                                '采购组_head', '需求来源','PortalTitle', '研发项目', '需求人', '结案类型', '结案日期',
                                '采购执行数量', '供应商', '明细主键', '单据状态']]
    sub_unpushed_today_data = sub_unpushed_detail_data[['超期状态','超期天数', '明细主键']]\
                                                      [sub_unpushed_detail_data['统计日期'] == today]
    sub_unpushed_today_detail_data = pd.merge(sub_clean_data, sub_unpushed_today_data,
                                              left_on='明细主键',
                                              right_on='明细主键',
                                              how='inner')

    sub_unpushed_today_detail_data = sub_unpushed_today_detail_data[
                               ['采购员_head','采购组_head','超期天数','超期状态','单据编号', '单据类型', 'sub审核/计划开工日期',
                                'sub审核日期','计划开工时间','用途', '物料编码', '物料名称','规格型号', '单位', '数量', '业务状态',
                                '计划开工时间', '计划完工时间','需求单据',  '需求来源', 'PortalTitle', '研发项目', '需求人',
                                '结案类型', '结案日期','采购执行数量', '供应商',  '单据状态','明细主键',]
                                     ].sort_values(by='超期天数',ascending=False)






    recivers_detail = {}   ###sub_unpushed_today_detail_data
    recivers_detail['序号'] = []
    recivers_detail['姓名'] = []
    recivers_detail['邮箱'] = []
    recivers_detail['明细表'] = []
    recivers_detail['HTML文件'] = []
    recivers_detail['图片'] = []

    """生成部门未下推明细表"""
    excel_path = data_clean.unpushed_data_to_excel(pr_unpushed_today_detail_data, sub_unpushed_today_detail_data, data_dir)
    recivers_detail['序号'].append(1)
    recivers_detail['姓名'].append('leader')
    recivers_detail['邮箱'].append(config('LEADERS'))
    recivers_detail['明细表'].append(excel_path)
    recivers_detail['HTML文件'].append(html_path)
    recivers_detail['图片'].append(image_filename)


    for i, se in enumerate(se_list):
        excel_path =data_clean.unpushed_data_to_excel(pr_unpushed_today_detail_data, 
                                                      sub_unpushed_today_detail_data, 
                                                      data_dir,se)
        recivers_detail['序号'].append(i+2)
        recivers_detail['姓名'].append(se)
        recivers_detail['邮箱'].append(recivers_email[i])
        recivers_detail['明细表'].append(excel_path)
        recivers_detail['HTML文件'].append(html_path)
        recivers_detail['图片'].append(image_filename)
    
    recivers_detail_df = pd.DataFrame(recivers_detail)
    recivers_detail_name = 'recivers_detail.xlsx'
    recivers_detail_path = os.path.join(data_dir,recivers_detail_name)
    recivers_detail_df.to_excel(recivers_detail_path, sheet_name='recivers',index=False)
