from Get_Kingdee_Data import Get_Kingdee_Data
import pandas as pd
import numpy as np
import datetime
import dateutil
from decouple import config
import  os



M = Get_Kingdee_Data()
po_order_raw = M.purchase_order
po_order = po_order_raw[['源单分录内码02', '源单编号02', '创建日期', '采购数量', '物料编码02', 'PO审核日期', '采购组02', '采购员02']]
po_order = po_order.copy()
po_order['PO审核日期'] = pd.to_datetime(po_order['PO审核日期'], errors='coerce', format="%Y/%m/%d").dt.date
po_order['创建日期'] = pd.to_datetime(po_order['创建日期'], errors='coerce', format="%Y/%m/%d").dt.date

today = datetime.datetime.today()
today = today.date()
# begin_date = today + dateutil.relativedelta.relativedelta(months=-4)
#
# pr_order_raw['pr审核日期'] = pd.to_datetime(pr_order_raw['pr审核日期'], errors='coerce', format="%Y/%m/%d").dt.date
# SE_group_data = pr_order_raw[['Sourcing', '采购组_head']][pr_order_raw['pr审核日期'] >= begin_date]
# SE_group_data = SE_group_data.drop_duplicates()



def pr_clean():
    #数据状态：A创建，B审核中，C已审核，D重新审核，Z暂存
    #业务关闭：A正常，B关闭
    #关闭状态：A正常，B关闭

    """                         清洗pr单                                    """
    pr_order_raw = M.purchase_requisition_order
    # pr_order = pr_order_raw[['物料编码', 'Sourcing', '单据编号', '数据状态', '关闭状态', 'pr审核日期', '建议采购日期', '业务终止',
    #                          '终止日期', '业务关闭', '关闭日期', '明细主键', '申请数量', '订单关联数量', '订单关联数量（基本单位）',
    #                          '采购组_head', '采购员_head','批准数量']]
    #
    pr_order = pr_order_raw[['单据编号', 'pr审核日期', '用途', '物料编码', '物料名称', '规格型号', '申请单位', '批准数量','申请数量',
                             '到货日期', '提前期', '建议采购日期', '采购员_head', 'Sourcing', '采购组_head', '需求来源','关闭日期',
                             '需求单据编号','PortalTitle', '研发项目', '需求人', '明细主键', '终止日期','业务关闭',
                             '业务终止','数据状态','订单关联数量','关闭状态']]


    """选取数据状态为已审核的数据"""
    pr_order = pr_order[pr_order['数据状态']=='C']
    """pr表与po表关联，获取PO表中的PO审核日期作为pr表中的业务关闭的日期"""
    pr_detail_data = pd.merge(pr_order,po_order, left_on='明细主键', right_on='源单分录内码02', how='left')

    pr_clean_data = pr_detail_data.copy()
    """计算订单差额，用于判别是否已下推"""
    pr_clean_data['订单差额'] = pr_clean_data['订单关联数量'] - pr_clean_data['批准数量']

    """整理日期格式"""
    replace_date = pd.to_datetime('1900-01-01',errors='coerce', format="%Y/%m/%d").date()
    pr_clean_data['pr审核日期'] = np.where(pr_clean_data['pr审核日期'].notnull(),
                                     pd.to_datetime(pr_clean_data['pr审核日期'],
                                                    errors='coerce',
                                                    format="%Y/%m/%d").dt.date, replace_date)

    pr_clean_data['建议采购日期'] = pd.to_datetime(pr_clean_data['建议采购日期'], errors='coerce', format="%Y/%m/%d").dt.date
    pr_clean_data['终止日期'] = pd.to_datetime(pr_clean_data['终止日期'], errors='coerce', format="%Y/%m/%d").dt.date
    pr_clean_data['关闭日期'] = pd.to_datetime(pr_clean_data['终止日期'], errors='coerce', format="%Y/%m/%d").dt.date

    """对于一批物料，分两次下推的问题，取创建日期最晚的时间，PO单的创建日期可以认为是pr单的下推日期"""
    pr_clean_data = pr_clean_data.sort_values(by='PO审核日期')
    pr_clean_data = pr_clean_data.drop_duplicates(subset=['明细主键'], keep='last').reset_index(drop=True)

    """pr审核日期、建议日期，取两者中最晚的日期"""
    max_date_list = []
    for i in range(len(pr_clean_data['建议采购日期'])):
        if pr_clean_data['建议采购日期'].iloc[i] >= pr_clean_data['pr审核日期'].iloc[i]:
            max_date_list.append(pr_clean_data['建议采购日期'].iloc[i])
        else:
            max_date_list.append(pr_clean_data['pr审核日期'].iloc[i])
    pr_clean_data['审核/建议日期'] = max_date_list


    return pr_clean_data




def get_statistical_date_list():  ##注意，是否只算工作日？

    """生成日期序列"""
    today = datetime.datetime.today()
    today = today.date()
    begin_date = today +  dateutil.relativedelta.relativedelta(months=-2)
    days = today - begin_date
    days = days.days
    statistical_date_list = [today + datetime.timedelta(days=i * (-1)) for i in range(days+1)]
    statistical_date_list.reverse()

    return  statistical_date_list


def get_pr_unpushed_data(pr_clean ):
    statistical_date_list = get_statistical_date_list()

    pr_detail_data = pr_clean
    pr_detail_data = pr_detail_data[['批准数量',  'Sourcing', 'PO审核日期','订单关联数量','审核/建议日期','pr审核日期','业务关闭',
                                     '终止日期' ,'明细主键']]

    pr_unpushed_dict = {}
    pr_unpushed_dict['统计日期'] = []
    pr_unpushed_dict['超期状态'] = []
    pr_unpushed_dict['超期天数'] = []
    pr_unpushed_dict['超期标识'] = []
    pr_unpushed_dict['明细主键'] = []
    pr_unpushed_dict['Sourcing'] = []
    pr_unpushed_dict['审核/建议日期'] = []


    for date in statistical_date_list:
        for i in range(len(pr_detail_data['明细主键'])):
            if pr_detail_data['订单关联数量'].iloc[i] >= pr_detail_data['批准数量'].iloc[i]:
                """审核日期02为空，代表未下推"""
                if pd.isna(pr_detail_data['PO审核日期'].iloc[i]):
                    if pr_detail_data['pr审核日期'].iloc[i] > date:
                        continue
                    else:
                        date_delta_days =  date - pr_detail_data['审核/建议日期'].iloc[i]

                        if (date_delta_days.days > 0):
                            overdue_flag = 1
                            overdue_status = '超期'
                        elif (date_delta_days.days <=0) & (date_delta_days.days >= -7):
                            overdue_flag = -1
                            overdue_status = '7日内将超期'
                        else:
                            overdue_flag = -2
                            overdue_status = '7日后将超期'

                        pr_unpushed_dict['统计日期'].append(date)
                        pr_unpushed_dict['超期状态'].append(overdue_status)
                        pr_unpushed_dict['超期天数'].append(date_delta_days.days)
                        pr_unpushed_dict['超期标识'].append(overdue_flag)
                        pr_unpushed_dict['明细主键'].append(pr_detail_data['明细主键'].iloc[i])
                        pr_unpushed_dict['Sourcing'].append(pr_detail_data['Sourcing'].iloc[i])
                        pr_unpushed_dict['审核/建议日期'].append(pr_detail_data['审核/建议日期'].iloc[i])

                else:
                    if pr_detail_data['PO审核日期'].iloc[i] <= date:

                        continue
                    else:
                        if pr_detail_data['pr审核日期'].iloc[i] > date:
                            continue
                        else:
                            date_delta_days = date - pr_detail_data['审核/建议日期'].iloc[i]

                            if (date_delta_days.days > 0):
                                overdue_flag = 1
                                overdue_status = '超期'
                            elif (date_delta_days.days <= 0) & (date_delta_days.days >= -7):
                                overdue_flag = -1
                                overdue_status = '7日内将超期'
                            else:
                                overdue_flag = -2
                                overdue_status = '7日后将超期'

                            pr_unpushed_dict['统计日期'].append(date)
                            pr_unpushed_dict['超期状态'].append(overdue_status)
                            pr_unpushed_dict['超期天数'].append(date_delta_days.days)
                            pr_unpushed_dict['超期标识'].append(overdue_flag)
                            pr_unpushed_dict['明细主键'].append(pr_detail_data['明细主键'].iloc[i])
                            pr_unpushed_dict['Sourcing'].append(pr_detail_data['Sourcing'].iloc[i])
                            pr_unpushed_dict['审核/建议日期'].append(pr_detail_data['审核/建议日期'].iloc[i])

            else:
                if pd.isna(pr_detail_data['终止日期'].iloc[i]):
                    if pr_detail_data['业务关闭'].iloc[i] == 'B':
                        continue

                    if pr_detail_data['pr审核日期'].iloc[i] > date:
                        continue
                    else:
                        date_delta_days = date - pr_detail_data['审核/建议日期'].iloc[i]

                        if (date_delta_days.days > 0):
                            overdue_flag = 1
                            overdue_status = '超期'
                        elif (date_delta_days.days <= 0) & (date_delta_days.days >= -7):
                            overdue_flag = -1
                            overdue_status = '7日内将超期'
                        else:
                            overdue_flag = -2
                            overdue_status = '7日后将超期'

                        pr_unpushed_dict['统计日期'].append(date)
                        pr_unpushed_dict['超期状态'].append(overdue_status)
                        pr_unpushed_dict['超期天数'].append(date_delta_days.days)
                        pr_unpushed_dict['超期标识'].append(overdue_flag)
                        pr_unpushed_dict['明细主键'].append(pr_detail_data['明细主键'].iloc[i])
                        pr_unpushed_dict['Sourcing'].append(pr_detail_data['Sourcing'].iloc[i])
                        pr_unpushed_dict['审核/建议日期'].append(pr_detail_data['审核/建议日期'].iloc[i])
                else:
                    if pr_detail_data['终止日期'].iloc[i] <= date:
                        continue
                    else:
                        if pr_detail_data['pr审核日期'].iloc[i] > date:
                            continue
                        else:
                            date_delta_days = date - pr_detail_data['审核/建议日期'].iloc[i]

                            if (date_delta_days.days > 0):
                                overdue_flag = 1
                                overdue_status = '超期'
                            elif (date_delta_days.days <= 0) & (date_delta_days.days >= -7):
                                overdue_flag = -1
                                overdue_status = '7日内将超期'
                            else:
                                overdue_flag = -2
                                overdue_status = '7日后将超期'
                            pr_unpushed_dict['统计日期'].append(date)
                            pr_unpushed_dict['超期状态'].append(overdue_status)
                            pr_unpushed_dict['超期天数'].append(date_delta_days.days)
                            pr_unpushed_dict['超期标识'].append(overdue_flag)
                            pr_unpushed_dict['明细主键'].append(pr_detail_data['明细主键'].iloc[i])
                            pr_unpushed_dict['Sourcing'].append(pr_detail_data['Sourcing'].iloc[i])
                            pr_unpushed_dict['审核/建议日期'].append(pr_detail_data['审核/建议日期'].iloc[i])

    pr_unpushed_detail_data = pd.DataFrame(pr_unpushed_dict)



    return pr_unpushed_detail_data


def get_pr_daily_total_detail(pr_clean):
    today = datetime.datetime.today()
    today = today.date()
    """限定数据范围，从当期日期开始，往前推100天的数据"""
    begin_date = today + datetime.timedelta(days=-100)
    pr_detail_data = pr_clean
    """获取新增未下推数据"""
    pr_unpushed_data = pr_detail_data[['明细主键', '单据编号', '物料编码', 'Sourcing', 'pr审核日期', '建议采购日期',
                                 '批准数量', 'PO审核日期', '业务关闭', '订单关联数量', '终止日期', '业务终止' ]] \
                                  [ (pr_detail_data['pr审核日期'] >= begin_date)]


    """获取已处理数据"""
    pr_processed_data_1 = pr_detail_data[['明细主键', '单据编号', '物料编码', 'Sourcing', 'pr审核日期', '建议采购日期', '订单差额',
                                     '批准数量', 'PO审核日期', '业务关闭', '订单关联数量', '终止日期', '业务终止', '关闭状态']] \
                                     [(pr_detail_data['订单差额'] >=0 ) & (pr_detail_data['PO审核日期'] >= begin_date)]
    pr_processed_data_1['已处理日期'] = pr_processed_data_1['PO审核日期']
    pr_processed_data_2 = pr_detail_data[['明细主键', '单据编号', '物料编码', 'Sourcing', 'pr审核日期', '建议采购日期', '订单差额',
                                        '批准数量', 'PO审核日期', '业务关闭', '订单关联数量', '终止日期', '业务终止', '关闭状态']] \
                                  [(pr_detail_data['订单差额'] < 0 ) & (pr_detail_data['终止日期'] >= begin_date)]
    pr_processed_data_2['已处理日期'] = pr_processed_data_2['终止日期']

    pr_processed_data = pd.concat([pr_processed_data_1, pr_processed_data_2], axis=0)


    """汇总计算新增未下推数据"""
    pr_daily_added_unpushed_total = pr_unpushed_data.groupby(['Sourcing', 'pr审核日期'])['明细主键'].count()

    """汇总计算已处理数据"""
    pr_daily_processed_total = pr_processed_data.groupby(['Sourcing', '已处理日期'])['明细主键'].count()

    """计算30日移动平均数"""
    statistical_date_list = get_statistical_date_list()
    pr_dep_moving_average = pd.DataFrame(columns=['近30日平均新增行数', '近30日平均已下推行数', '统计日期'])
    for date in statistical_date_list:
        """从统计日期开始，计算前30天的平均数"""
        start_date = date + datetime.timedelta(days=-30)
        """计算pr单近30日平均新增行数"""

        pr_unpushed_moving_data = pr_daily_added_unpushed_total[(pr_daily_added_unpushed_total.index.get_level_values(1) >
                                                       start_date)
                                                 & (pr_daily_added_unpushed_total.index.get_level_values(1) <= date)
                                                ]
        pr_unpushed_moving_total = pr_unpushed_moving_data.groupby(level='Sourcing').sum()
        pr_unpushed_moving_avg_30days = pr_unpushed_moving_total / 30
        pr_unpushed_total_average = pr_unpushed_moving_avg_30days.sum()
        pr_unpushed_total_average_series = pd.Series([pr_unpushed_total_average], index=['总计'])
        pr_unpushed_moving_avg_30days = pd.concat([pr_unpushed_moving_avg_30days, pr_unpushed_total_average_series], axis=0)
        pr_unpushed_moving_avg_30days = pr_unpushed_moving_avg_30days.rename('近30日平均新增行数')


        """计算pr近30日平均已下推行数"""
        pr_processed_moving_data = pr_daily_processed_total[(pr_daily_processed_total.index.get_level_values(1) >
                                                       start_date)
                                                 & (pr_daily_processed_total.index.get_level_values(1) <= date)]
        pr_processed_moving_total = pr_processed_moving_data.groupby(level='Sourcing').sum()
        pr_processed_moving_avg_30days = pr_processed_moving_total / 30
        pr_processed_total_average = pr_processed_moving_avg_30days.sum()
        pr_processed_total_average_series = pd.Series([pr_processed_total_average], index=['总计'])
        pr_processed_moving_avg_30days = pd.concat([pr_processed_moving_avg_30days, pr_processed_total_average_series], axis=0)
        pr_processed_moving_avg_30days = pr_processed_moving_avg_30days.rename('近30日平均已下推行数')


        pr_moving_avg_30days_data = pd.concat([pr_unpushed_moving_avg_30days, pr_processed_moving_avg_30days], axis=1)
        pr_moving_avg_30days_data['统计日期'] = date

        pr_dep_moving_average = pd.concat([pr_dep_moving_average, pr_moving_avg_30days_data], axis=0)

    pr_dep_moving_average = pr_dep_moving_average.reset_index(drop=False)
    pr_dep_moving_average = pr_dep_moving_average.rename(columns={'index':'Sourcing'})



    """将每日新增总量拼接到上表中"""
    pr_dep_total_unpushed = pr_daily_added_unpushed_total.groupby(level='pr审核日期').sum()
    pr_dep_total_unpushed = pr_dep_total_unpushed.reset_index(drop=False)
    pr_dep_total_unpushed = pr_dep_total_unpushed.reindex(columns=['Sourcing', 'pr审核日期', '明细主键'], fill_value='总计')
    pr_daily_added_unpushed_total = pr_daily_added_unpushed_total.reset_index(drop=False)
    pr_daily_added_unpushed_total = pd.concat([pr_daily_added_unpushed_total, pr_dep_total_unpushed],axis=0)
    pr_daily_added_unpushed_total = pr_daily_added_unpushed_total.rename(columns={'明细主键': '每日新增行数'})

    pr_daily_total_detail = pd.merge(pr_dep_moving_average, pr_daily_added_unpushed_total,
                                  left_on=['Sourcing', '统计日期'],
                                  right_on=['Sourcing', 'pr审核日期'],
                                  how='left'
                                  )

    pr_daily_total_detail['每日新增行数'] = pr_daily_total_detail['每日新增行数'].fillna(0)

    return pr_daily_total_detail



"""                              清洗委外订单                             """
def sub_clean():

    sub_order_raw = M.sub_subreqorder_order
    # sub_order = sub_order_raw[['明细主键', '单据编号', '单据状态', '物料编码','物料名称', '规格型号','单位','数量','委外组织', '计划开工时间',
    #                            '供应商',
    #                            '用途']]
    sub_order = sub_order_raw[['单据编号', '单据类型', 'sub审核日期', '用途', '物料编码', '物料名称', '规格型号', '单位', '数量',
                               '业务状态', '计划开工时间', '计划完工时间', '需求单据', '采购员_head', '采购组_head', '需求来源',
                               'PortalTitle','研发项目', '需求人', '结案类型', '结案日期','采购执行数量','供应商','明细主键',
                               '单据状态']]


    """选取单据状态为已审核的数据"""
    sub_order = sub_order[sub_order['单据状态'] == 'C']

    """sub表与po表关联，获取po表中的PO审核日日期作为sub表中的下推日期"""
    sub_pr_detail_data = pd.merge(sub_order, po_order, left_on='明细主键', right_on='源单分录内码02', how='left')

    sub_clean_data = sub_pr_detail_data.copy()
    """计算订单差额，用于判别是否已下推"""
    sub_clean_data['订单差额'] = sub_clean_data['采购执行数量'] - sub_clean_data['数量']

    """整理日期格式"""
    replace_date = pd.to_datetime('1900-01-01', errors='coerce', format="%Y/%m/%d").date()
    sub_clean_data['sub审核日期'] = np.where(sub_clean_data['sub审核日期'].notnull(),
                                    pd.to_datetime(sub_clean_data['sub审核日期'],
                                                   errors='coerce',
                                                   format="%Y/%m/%d").dt.date, replace_date)

    sub_clean_data['计划开工时间'] = pd.to_datetime(sub_clean_data['计划开工时间'], errors='coerce', format="%Y/%m/%d").dt.date
    sub_clean_data['结案日期'] = pd.to_datetime(sub_clean_data['结案日期'], errors='coerce', format="%Y/%m/%d").dt.date

    """对于一批物料，分两次下推的问题，取创建日期最晚的时间，PO单的创建日期可以认为是sub单的下推日期"""
    sub_clean_data = sub_clean_data.sort_values(by='PO审核日期')
    sub_clean_data = sub_clean_data.drop_duplicates(subset=['明细主键'], keep='last').reset_index(drop=True)

    """计划开工时间、sub审核日期，取两者中最晚的日期"""
    max_date_list = []
    for i in range(len(sub_clean_data['计划开工时间'])):
        if sub_clean_data['计划开工时间'].iloc[i] >= sub_clean_data['sub审核日期'].iloc[i]:
            max_date_list.append(sub_clean_data['计划开工时间'].iloc[i])
        else:
            max_date_list.append(sub_clean_data['sub审核日期'].iloc[i])
    sub_clean_data['sub审核/计划开工日期'] = max_date_list

    return sub_clean_data


def get_sub_unpushed_data(sub_clean):
    """获取委外未下推订单"""
    statistical_date_list = get_statistical_date_list()
    sub_detail_data = sub_clean

    sub_unpushed_dict = {}
    sub_unpushed_dict['统计日期'] = []
    sub_unpushed_dict['超期状态'] = []
    sub_unpushed_dict['超期天数'] = []
    sub_unpushed_dict['超期标识'] = []
    sub_unpushed_dict['明细主键'] = []
    sub_unpushed_dict['Sourcing'] = []
    sub_unpushed_dict['sub审核/计划开工日期'] = []

    for date in statistical_date_list:
        for i in range(len(sub_detail_data['明细主键'])):
            if sub_detail_data['采购执行数量'].iloc[i] >= sub_detail_data['数量'].iloc[i]:
                """审核日期02为空，代表未下推"""
                if pd.isna(sub_detail_data['PO审核日期'].iloc[i]):
                    if sub_detail_data['sub审核日期'].iloc[i] > date:
                        continue
                    else:
                        date_delta_days = date - sub_detail_data['sub审核/计划开工日期'].iloc[i]

                        if (date_delta_days.days > 0):
                            overdue_flag = 1
                            overdue_status = '超期'
                        elif (date_delta_days.days <= 0) & (date_delta_days.days >= -7):
                            overdue_status = '7日内将超期'
                            overdue_flag = -1
                        else:
                            overdue_status = '7日后将超期'
                            overdue_flag = -2

                        sub_unpushed_dict['统计日期'].append(date)
                        sub_unpushed_dict['超期状态'].append(overdue_status)
                        sub_unpushed_dict['超期天数'].append(date_delta_days.days)
                        sub_unpushed_dict['超期标识'].append(overdue_flag)
                        sub_unpushed_dict['明细主键'].append(sub_detail_data['明细主键'].iloc[i])
                        sub_unpushed_dict['Sourcing'].append(sub_detail_data['采购员_head'].iloc[i])
                        sub_unpushed_dict['sub审核/计划开工日期'].append(sub_detail_data['sub审核/计划开工日期'].iloc[i])

                else:
                    if sub_detail_data['PO审核日期'].iloc[i] <= date:
                        continue
                    else:
                        if sub_detail_data['sub审核日期'].iloc[i] > date:
                            continue
                        else:
                            date_delta_days = date - sub_detail_data['sub审核/计划开工日期'].iloc[i]

                            if (date_delta_days.days > 0):
                                overdue_flag = 1
                                overdue_status = '超期'
                            elif (date_delta_days.days <= 0) & (date_delta_days.days >= -7):
                                overdue_flag = -1
                                overdue_status = '7日内将超期'
                            else:
                                overdue_flag = -2
                                overdue_status = '7日后将超期'

                            sub_unpushed_dict['统计日期'].append(date)
                            sub_unpushed_dict['超期状态'].append(overdue_status)
                            sub_unpushed_dict['超期天数'].append(date_delta_days.days)
                            sub_unpushed_dict['超期标识'].append(overdue_flag)
                            sub_unpushed_dict['明细主键'].append(sub_detail_data['明细主键'].iloc[i])
                            sub_unpushed_dict['Sourcing'].append(sub_detail_data['采购员_head'].iloc[i])
                            sub_unpushed_dict['sub审核/计划开工日期'].append(sub_detail_data['sub审核/计划开工日期'].iloc[i])

            else:
                if pd.isna(sub_detail_data['结案日期'].iloc[i]):

                    if sub_detail_data['采购执行数量'].iloc[i] >0:   #委外订单只下推一次，只要采购执行数量>0就可下推
                        continue
                    if sub_detail_data['sub审核日期'].iloc[i] > date:
                        continue
                    else:
                        date_delta_days = date - sub_detail_data['sub审核/计划开工日期'].iloc[i]

                        if (date_delta_days.days > 0):
                            overdue_flag = 1
                            overdue_status = '超期'
                        elif (date_delta_days.days <= 0) & (date_delta_days.days >= -7):
                            overdue_flag = -1
                            overdue_status = '7日内将超期'
                        else:
                            overdue_flag = -2
                            overdue_status = '7日后将超期'

                        sub_unpushed_dict['统计日期'].append(date)
                        sub_unpushed_dict['超期状态'].append(overdue_status)
                        sub_unpushed_dict['超期天数'].append(date_delta_days.days)
                        sub_unpushed_dict['超期标识'].append(overdue_flag)
                        sub_unpushed_dict['明细主键'].append(sub_detail_data['明细主键'].iloc[i])
                        sub_unpushed_dict['Sourcing'].append(sub_detail_data['采购员_head'].iloc[i])
                        sub_unpushed_dict['sub审核/计划开工日期'].append(sub_detail_data['sub审核/计划开工日期'].iloc[i])
                else:
                    if sub_detail_data['结案日期'].iloc[i] <= date:
                        continue
                    else:
                        if sub_detail_data['sub审核日期'].iloc[i] > date:
                            continue
                        else:
                            date_delta_days = date - sub_detail_data['sub审核/计划开工日期'].iloc[i]

                            if (date_delta_days.days > 0):
                                overdue_flag = 1
                                overdue_status = '超期'
                            elif (date_delta_days.days <= 0) & (date_delta_days.days >= -7):
                                overdue_flag = -1
                                overdue_status = '7日内将超期'
                            else:
                                overdue_flag = -2
                                overdue_status = '7日后将超期'

                            sub_unpushed_dict['统计日期'].append(date)
                            sub_unpushed_dict['超期状态'].append(overdue_status)
                            sub_unpushed_dict['超期天数'].append(date_delta_days.days)
                            sub_unpushed_dict['超期标识'].append(overdue_flag)
                            sub_unpushed_dict['明细主键'].append(sub_detail_data['明细主键'].iloc[i])
                            sub_unpushed_dict['Sourcing'].append(sub_detail_data['采购员_head'].iloc[i])
                            sub_unpushed_dict['sub审核/计划开工日期'].append(sub_detail_data['sub审核/计划开工日期'].iloc[i])
    sub_unpushed_detail_data = pd.DataFrame(sub_unpushed_dict)

    return sub_unpushed_detail_data

def get_sub_daily_total_detail(sub_clean):
    today = datetime.datetime.today()
    today = today.date()
    """限定数据范围，从当期日期开始，往前推100天的数据"""
    begin_date = today + datetime.timedelta(days=-100)               #dateutil.relativedelta.relativedelta(months=-3)
    sub_detail_data = sub_clean
    sub_detail_data = sub_detail_data.rename(columns={'采购员_head': 'Sourcing'})
    """获取新增未下推数据"""
    sub_unpushed_data = sub_detail_data[['明细主键', '单据编号', '物料编码', 'Sourcing', 'sub审核日期', '计划开工时间',
                                         '数量', 'PO审核日期', '结案类型', '采购执行数量', '结案日期']] \
                                       [ (sub_detail_data['sub审核日期'] >= begin_date)]


    """获取已处理数据"""
    sub_processed_data_1 = sub_detail_data[['明细主键', '单据编号', '物料编码', 'Sourcing', 'sub审核日期', '计划开工时间',
                                            '数量', 'PO审核日期', '结案类型', '采购执行数量', '结案日期']] \
                                   [(sub_detail_data['订单差额'] >=0 ) & (sub_detail_data['PO审核日期'] >= begin_date)]
    sub_processed_data_1['已处理日期'] = sub_processed_data_1['PO审核日期']

    sub_processed_data_2 = sub_detail_data[['明细主键', '单据编号', '物料编码', 'Sourcing', 'sub审核日期', '计划开工时间',
                                            '数量', 'PO审核日期', '结案类型', '采购执行数量', '结案日期']] \
                                   [(sub_detail_data['订单差额'] < 0 ) & (sub_detail_data['结案日期'] >= begin_date)]

    sub_processed_data_2['已处理日期'] = sub_processed_data_2['结案日期']

    sub_processed_data = pd.concat([sub_processed_data_1, sub_processed_data_2], axis=0)


    """汇总计算新增未下推数据"""
    sub_daily_added_unpushed_total = sub_unpushed_data.groupby(['Sourcing', 'sub审核日期'])['明细主键'].count()

    """汇总计算已处理数据"""
    sub_daily_processed_total = sub_processed_data.groupby(['Sourcing', '已处理日期'])['明细主键'].count()

    """计算30日移动平均数"""
    statistical_date_list = get_statistical_date_list()
    sub_dep_moving_average = pd.DataFrame(columns=['近30日平均新增行数', '近30日平均已下推行数', '统计日期'])
    for date in statistical_date_list:
        """从统计日期开始，计算前30天的平均数"""
        start_date = date + datetime.timedelta(days=-30)
        """计算sub单近30日平均新增行数"""

        sub_unpushed_moving_data = sub_daily_added_unpushed_total[(sub_daily_added_unpushed_total.index.get_level_values(1) >
                                                       start_date)
                                                 & (sub_daily_added_unpushed_total.index.get_level_values(1) <= date)
                                                ]
        sub_unpushed_moving_total = sub_unpushed_moving_data.groupby(level='Sourcing').sum()
        sub_unpushed_moving_avg_30days = sub_unpushed_moving_total / 30
        sub_unpushed_total_average = sub_unpushed_moving_avg_30days.sum()
        sub_unpushed_total_average_series = pd.Series([sub_unpushed_total_average], index=['总计'])
        sub_unpushed_moving_avg_30days = pd.concat([sub_unpushed_moving_avg_30days, sub_unpushed_total_average_series],
                                                  axis=0)
        sub_unpushed_moving_avg_30days = sub_unpushed_moving_avg_30days.rename('近30日平均新增行数')


        """计算pr近30日平均已下推行数"""
        sub_processed_moving_data = sub_daily_processed_total[(sub_daily_processed_total.index.get_level_values(1) >
                                                       start_date)
                                                 & (sub_daily_processed_total.index.get_level_values(1) <= date)]
        sub_processed_moving_total = sub_processed_moving_data.groupby(level='Sourcing').sum()
        sub_processed_moving_avg_30days = sub_processed_moving_total / 30
        sub_processed_total_average = sub_processed_moving_avg_30days.sum()
        sub_processed_total_average_series = pd.Series([sub_processed_total_average], index=['总计'])
        sub_processed_moving_avg_30days = pd.concat([sub_processed_moving_avg_30days,
                                                   sub_processed_total_average_series], axis=0)
        sub_processed_moving_avg_30days = sub_processed_moving_avg_30days.rename('近30日平均已下推行数')


        sub_moving_avg_30days_data = pd.concat([sub_unpushed_moving_avg_30days, sub_processed_moving_avg_30days],
                                               axis=1)
        sub_moving_avg_30days_data['统计日期'] = date

        sub_dep_moving_average = pd.concat([sub_dep_moving_average, sub_moving_avg_30days_data], axis=0)

    sub_dep_moving_average = sub_dep_moving_average.reset_index(drop=False)
    sub_dep_moving_average = sub_dep_moving_average.rename(columns={'index':'Sourcing'})



    """将每日新增总量拼接到上表中"""
    sub_dep_total_unpushed = sub_daily_added_unpushed_total.groupby(level='sub审核日期').sum()
    sub_dep_total_unpushed = sub_dep_total_unpushed.reset_index(drop=False)
    sub_dep_total_unpushed = sub_dep_total_unpushed.reindex(columns=['Sourcing', 'sub审核日期', '明细主键'], fill_value='总计')
    sub_daily_added_unpushed_total = sub_daily_added_unpushed_total.reset_index(drop=False)
    sub_daily_added_unpushed_total = pd.concat([sub_daily_added_unpushed_total, sub_dep_total_unpushed],axis=0)
    sub_daily_added_unpushed_total = sub_daily_added_unpushed_total.rename(columns={'明细主键': '每日新增行数'})

    sub_daily_total_detail = pd.merge(sub_dep_moving_average, sub_daily_added_unpushed_total,
                                  left_on=['Sourcing', '统计日期'],
                                  right_on=['Sourcing', 'sub审核日期'],
                                  how='left'
                                  )

    sub_daily_total_detail['每日新增行数'] = sub_daily_total_detail['每日新增行数'].fillna(0)
    # sub_daily_total_detail.to_excel(r'.\数据分析\sub_daily_total_detail.xlsx', sheet_name='sub_daily_total_detail')
    return sub_daily_total_detail


def unpushed_data_to_excel(pr_unpushed, sub_unpushed,data_dir, se=None):
    path = ''
    if se != None:
        pr_unpushed_detail = pr_unpushed[pr_unpushed['Sourcing'] == se]
        sub_unpushed_detail = sub_unpushed[sub_unpushed['采购员_head'] == se]
        if (not pr_unpushed_detail.empty) or (not sub_unpushed_detail.empty):
            excel_name = config('FORM_NAME').format(se)
            path = os.path.join(data_dir, excel_name)
            writer = pd.ExcelWriter(path, engine='xlsxwriter')
            workbook = writer.book
            format = workbook.add_format()
            format.set_align('center')
            format.set_align('vcenter')

            if not pr_unpushed_detail.empty:
                pr_unpushed_detail.to_excel(writer, sheet_name='PR未下推明细',index=False)
                """将超期日期标红"""

                worksheet = writer.sheets['PR未下推明细']


                worksheet.set_column('A:AF', 15,format)
                worksheet.set_top_left_cell()

                #text_color = workbook.add_format({'color': 'red'})
                # for i in range(len(pr_unpushed_detail['审核/建议日期'])):
                #     if pr_unpushed_detail['审核/建议日期'].iloc[i] < today:
                #         print('A:',pr_unpushed_detail['审核/建议日期'].iloc[i])
                #         #worksheet.write(i+1,1,pr_unpushed_detail['审核/建议日期'].iloc[i], text_color)


                writer.save()

            if not sub_unpushed_detail.empty:

                sub_unpushed_detail.to_excel(writer, sheet_name='委外未下推明细',index=False)
                worksheet = writer.sheets['委外未下推明细']
                worksheet.set_column('A:AF', 15, format)


                writer.save()

    else:
        if (not pr_unpushed.empty) or (not sub_unpushed.empty):
            excel_name = config('FORM_NAME').format('').replace('_', '')
            path = os.path.join(data_dir, excel_name)
            writer = pd.ExcelWriter(path,  engine='xlsxwriter')
            workbook = writer.book
            format = workbook.add_format()
            format.set_align('center')
            format.set_align('vcenter')
            f = workbook.add_format({'align': 'vcenter'})
            if not pr_unpushed.empty:
                pr_unpushed.to_excel(writer, sheet_name='PR未下推明细',index=False)
                worksheet = writer.sheets['PR未下推明细']

                worksheet.set_column('A:AF', 15,format)
            if not sub_unpushed.empty:
                sub_unpushed.to_excel(writer, sheet_name='委外未下推明细',index=False)
                worksheet = writer.sheets['委外未下推明细']
                worksheet.set_column('A:AF', 15, format)
            writer.save()





    return  path
# get_sub_unpushed_data()
# get_sub_daily_total_detail()
# pr_clean()
