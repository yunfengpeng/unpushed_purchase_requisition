import data_clean
##

import pandas as pd
from pyecharts import charts
from pyecharts import options as opts

import datetime

from pyecharts.globals import ThemeType




class chart():
    def __init__(self):
        self.statistical_date = data_clean.get_statistical_date_list()
        # self.pr_daily_total_detail = data_clean.get_pr_daily_total_detail()
        # self.sub_daily_total_detail = data_clean.get_sub_daily_total_detail()
        # self.pr_unpushed_detail_data = data_clean.get_pr_unpushed_data()
        # self.sub_unpushed_detail_data = data_clean.get_sub_unpushed_data()
        # self.pr_clean_data = data_clean.pr_clean()
        # self.sub_clean_data = data_clean.sub_clean()



    def dep_remaining_unpushed_chart( self, unpushed_data, flag):

        unpushed_detail_data = unpushed_data
        dep_unpushed_total_data = unpushed_detail_data.groupby(['统计日期', '超期状态','超期标识'])['超期状态'].count()
        dep_unpushed_total_data = dep_unpushed_total_data.rename('数量')
        dep_unpushed_total_data = dep_unpushed_total_data.sort_index(level='超期标识', ascending=False)
        dep_unpushed_total_data = dep_unpushed_total_data.reset_index(drop=False)


        bar = charts.Bar()
        data_xaxis = self.statistical_date
        bar.add_xaxis(data_xaxis)
        statistical_data = pd.Series(self.statistical_date, name='统计日期_2')

        overdue_status_list = list(dep_unpushed_total_data['超期状态'].drop_duplicates())
        for i, status in enumerate(overdue_status_list):
            overdue_status = dep_unpushed_total_data[['统计日期', '数量']][dep_unpushed_total_data['超期状态']==status]
            """让每一种超期状态（包括0）都有一个统计日期与其对应，需关联整个统计日期"""
            overdue_status = pd.merge(statistical_data, overdue_status, left_on='统计日期_2', right_on='统计日期', how='left')
            overdue_status['数量'] = overdue_status['数量'].fillna(0)

            y = list(overdue_status['数量'])
            if status == '超期':
                color = '#E63F00 '    #橘红色
            elif status == '7日内将超期':
                color ='#FFBB00'      #黄色
            else:
                color ='#55AA00'      #绿色

            bar.add_yaxis(status, y, stack="stack", bar_width='60%', gap='10%',
                          itemstyle_opts=opts.ItemStyleOpts(color=color))

        bar.set_global_opts(title_opts=opts.TitleOpts(title= flag + '未下推订单部门总行数趋势图',
                                                      pos_left='center',
                                                      title_textstyle_opts=opts.TextStyleOpts(font_size=17)
                                                     ),
                            xaxis_opts=opts.AxisOpts(name='统计日期',
                                                     name_location='middle',
                                                     name_gap=60,
                                axislabel_opts=opts.LabelOpts(rotate=45)),
                            legend_opts=opts.LegendOpts( pos_top="5%")
                            )
        bar.set_series_opts(opts.LabelOpts(is_show=False))

        return bar

    def se_remaining_unpushed_chart(self, se, unpushed_data, daily_total_detail, flag, traversal_order=None):
        unpushed_detail_data = unpushed_data[unpushed_data['Sourcing'] == se]
        se_daily_total_detail = daily_total_detail[daily_total_detail['Sourcing'] == se]

        """添加各SE的剩余未下推趋势图"""
        se_unpushed_total_data = unpushed_detail_data.groupby(['Sourcing', '统计日期', '超期状态', '超期标识'])['超期状态'].count()
        se_unpushed_total_data = se_unpushed_total_data.rename('数量')
        se_unpushed_total_data = se_unpushed_total_data.sort_index(level=['超期标识'], ascending=[ False])
        se_unpushed_total_data = se_unpushed_total_data.reset_index(drop=False)


        bar = charts.Bar()
        data_xaxis = self.statistical_date
        bar.add_xaxis(data_xaxis)
        statistical_data = pd.Series(self.statistical_date, name='统计日期_2')
        overdue_status_list = list(se_unpushed_total_data['超期状态'].drop_duplicates())




        for i, status in enumerate(overdue_status_list):
            overdue_status = se_unpushed_total_data[['统计日期', '数量']][se_unpushed_total_data['超期状态'] == status]
            overdue_status = pd.merge(statistical_data, overdue_status, left_on='统计日期_2', right_on='统计日期', how='left')
            #overdue_status['数量'] = overdue_status['数量'].fillna(0)
            y = list(overdue_status['数量'])


            if status == '超期':
                color = '#E63F00 '  # 橘红色
            elif status == '7日内将超期':
                color = '#FFBB00'  # 黄色
            else:
                color = '#55AA00'  # 绿色

            bar.add_yaxis(status, y, stack=se, bar_width='60%', gap='10%',
                          itemstyle_opts=opts.ItemStyleOpts(color=color))

        if traversal_order == 0:
            bar.set_global_opts(title_opts=opts.TitleOpts(
                                                          title= flag + '未下推订单行数趋势图',

                                                          pos_left='center',
                                                          title_textstyle_opts=opts.TextStyleOpts(font_size=17)
                                                          ),
                                xaxis_opts=opts.AxisOpts(name='统计日期',
                                                         name_location='middle',
                                                         name_gap=60,
                                    axislabel_opts=opts.LabelOpts(rotate=45)),
                                yaxis_opts=opts.AxisOpts(name=se), ####
                                legend_opts=opts.LegendOpts(pos_top='30px',item_gap=30)  #该
                                )
        else:
            bar.set_global_opts(
                xaxis_opts=opts.AxisOpts(name='统计日期',
                                         name_location='middle',
                                         name_gap=60,
                    axislabel_opts=opts.LabelOpts(rotate=45)),
                yaxis_opts=opts.AxisOpts(name=se),  ####
                legend_opts=opts.LegendOpts(is_show=False)  # 该
            )

        bar.set_series_opts(opts.LabelOpts(is_show=False))
        """添加折线图"""
        line = charts.Line()
        y_1 = list(se_daily_total_detail['近30日平均新增行数'])

        y_2 = list(se_daily_total_detail['近30日平均已下推行数'])


        line.add_xaxis(data_xaxis)
        line.add_yaxis('近30日平均新增行数', y_1, itemstyle_opts=opts.ItemStyleOpts(color='#FF0 0FF'),
                                                  linestyle_opts = opts.LineStyleOpts(width=2, color='#FF0 0FF'))  # 紫色
        line.add_yaxis('近30日平均已下推行数', y_2, itemstyle_opts=opts.ItemStyleOpts(color='#5599FF'),
                                                  linestyle_opts = opts.LineStyleOpts(width=2, color='#5599FF'))  # 蓝色
        line.set_series_opts(opts.LabelOpts(is_show=False))
        bar.overlap(line)

        return bar

    def dep_daily_unpushed_processed_chart(self,daily_total_detail,flag):
        daily_total_detail = daily_total_detail[daily_total_detail['Sourcing']=='总计']
        data_xaxis = self.statistical_date

        y_1 = list(daily_total_detail['近30日平均新增行数'])
        y_2 = list(daily_total_detail['近30日平均已下推行数'])
        y_3 = list(daily_total_detail['每日新增行数'])

        line = (
                 charts.Line(init_opts=opts.InitOpts(
                                           #width='90%', height='10%'
                                              )
                   )
                 .add_xaxis(data_xaxis)
                 .add_yaxis('近30日平均新增行数', y_axis=y_1,
                                                 itemstyle_opts=opts.ItemStyleOpts(color="#CC00FF "),
                                                 linestyle_opts=opts.LineStyleOpts(width=2, color='#CC00FF '),
                       )
                 .add_yaxis('近30日平均已下推行数', y_axis=y_2,
                                                   itemstyle_opts=opts.ItemStyleOpts(color="#0066FF"),
                                                   linestyle_opts=opts.LineStyleOpts(width=2, color='#0066FF'),
                       )
                 .add_yaxis('每日新增行数', y_axis=y_3,
                                                   itemstyle_opts=opts.ItemStyleOpts(color="#888888"),
                                                   linestyle_opts=opts.LineStyleOpts(width=2, color='#888888'),
                       )
                 .set_series_opts(opts.LabelOpts(is_show=False
                                            )
                             )
                 .set_global_opts(title_opts=opts.TitleOpts(title=flag + '部门新增行数和已下推行数趋势图',
                                                            pos_top='50%',
                                                            pos_left='center',
                                                            title_textstyle_opts=opts.TextStyleOpts(font_size=17)
                                                            ),
                                  xaxis_opts=opts.AxisOpts(name='统计日期',
                                                           name_location='middle',
                                                           name_gap=60,
                                                           axislabel_opts=opts.LabelOpts(
                                      rotate=45)),
                                  legend_opts=opts.LegendOpts( pos_top="55%")
                                 )

               )

        return line

    """生成日期序列"""


    def suggested_purchase_date_chart(self, se , unpushed_detail_data, flag, traversal_order=None):
        today = datetime.datetime.today()
        today = today.date()
        bar = charts.Bar()

        if flag == 'PR':
            detail_data = unpushed_detail_data[['Sourcing', '审核/建议日期','超期状态']]\
                                    [(unpushed_detail_data['统计日期'] == today)
                                     & (unpushed_detail_data['Sourcing'] == se)]
            groupby_data = detail_data.groupby(['Sourcing', '审核/建议日期', '超期状态'])['超期状态'].count()
            groupby_data = groupby_data.rename('数量')
            groupby_data = groupby_data.reset_index(drop=False)

            if today not in groupby_data['审核/建议日期']:
                today_data = pd.DataFrame({'Sourcing': se, '审核/建议日期': today, '数量': 0}, index=[0])
                groupby_data = groupby_data.append(today_data,ignore_index=True)
            groupby_data = groupby_data.sort_values(by=['审核/建议日期']).reset_index(drop=False)

            data_xaxis = list(groupby_data['审核/建议日期'])
            groupby_data = groupby_data.rename(columns={'审核/建议日期':'审核日期'})

        else:
            detail_data = unpushed_detail_data[['Sourcing', 'sub审核/计划开工日期', '超期状态']] \
                                  [(unpushed_detail_data['统计日期'] == today)
                                   & (unpushed_detail_data['Sourcing'] == se)]

            groupby_data = detail_data.groupby(['Sourcing', 'sub审核/计划开工日期', '超期状态'])['超期状态'].count()
            groupby_data = groupby_data.rename('数量')
            groupby_data = groupby_data.reset_index(drop=False)
            if today not in groupby_data['sub审核/计划开工日期']:
                today_data = pd.DataFrame({'Sourcing':se,'sub审核/计划开工日期':today,'数量':0}, index=[0])
                groupby_data = groupby_data.append(today_data,ignore_index=True)

            groupby_data = groupby_data.sort_values(by=['sub审核/计划开工日期']).reset_index(drop=False)

            data_xaxis = list(groupby_data['sub审核/计划开工日期'])
            groupby_data = groupby_data.rename(columns={'sub审核/计划开工日期': '审核日期'})



        """按x轴给折线图分段边界索引"""

        bar.add_xaxis(data_xaxis)


        #overdue_status_list = list(groupby_data['超期状态'].drop_duplicates())
        overdue_status_list = ['超期', '7日内将超期', '7日后将超期']
        for i, status in enumerate(overdue_status_list):
            overdue_status_count = groupby_data[['审核日期', '数量']][groupby_data['超期状态'] == status]
            overdue_status_count = pd.merge(groupby_data['审核日期'], overdue_status_count,
                                            left_on='审核日期',right_on='审核日期', how='left')
            overdue_status_count['数量'] = overdue_status_count['数量']


            """让每一种超期状态（包括0）都有一个建议日期与其对应，需关联整个建议日期"""

            y = list(overdue_status_count['数量'])
            if status == '超期':
                color = '#E63F00 '  # 橘红色
            elif status == '7日内将超期':
                color = '#FFBB00'  # 黄色
            else:
                color = '#55AA00'  # 绿色

            bar.add_yaxis(status, y, stack=se, bar_width='60%', gap='10%',
                          itemstyle_opts=opts.ItemStyleOpts(color=color))
        if flag == 'PR':
            if traversal_order == 0:
                bar.set_global_opts(
                                title_opts=opts.TitleOpts(title=flag + '当日未下推订单行数分布图',
                                                          #subtitle='建议采购日期： 取建议采购日期与PR审核日期中最晚日期',
                                                          item_gap=40,
                                                          pos_left='center',
                                                          title_textstyle_opts=opts.TextStyleOpts(font_size=17),
                                                          subtitle_textstyle_opts=opts.TextStyleOpts(color='#444444')
                                                      ),
                                xaxis_opts=opts.AxisOpts(name='max( 建议采购日期, 审核日期 )',
                                                         name_location='middle',
                                                         name_gap=60,
                                                         axislabel_opts=opts.LabelOpts(rotate=45)
                                                         ),
                                yaxis_opts=opts.AxisOpts(name=se),
                                legend_opts=opts.LegendOpts(pos_top="30px")
                               )
            else:
                bar.set_global_opts(
                    xaxis_opts=opts.AxisOpts(name='max( 建议采购日期, 审核日期 )',
                                             name_location='middle',
                                             name_gap=60,
                                             axislabel_opts=opts.LabelOpts(rotate=45)),
                    yaxis_opts=opts.AxisOpts(name=se),
                    legend_opts=opts.LegendOpts(is_show=False)
                )

        else:
            if traversal_order == 0:
                bar.set_global_opts(
                                 title_opts=opts.TitleOpts(title=flag + '当日未下推订单行数分布图',
                                              #subtitle='建议采购日期： 取计划开工日期与委外审核日期中最晚日期',
                                              item_gap=40,
                                              pos_left='center',
                                              title_textstyle_opts=opts.TextStyleOpts(font_size=17),
                                              subtitle_textstyle_opts=opts.TextStyleOpts(color='#444444')
                                              ),
                                 xaxis_opts=opts.AxisOpts(name='max(计划开工日期, 审核日期)',
                                                          name_location='middle',
                                                          name_gap=60,
                                                          axislabel_opts=opts.LabelOpts(rotate=45)
                                                       ),
                                 yaxis_opts=opts.AxisOpts(name=se),
                                 legend_opts=opts.LegendOpts(pos_top="30px")
                                )

            else:
                bar.set_global_opts(
                    xaxis_opts=opts.AxisOpts(name='max(计划开工日期, 审核日期)',
                                             name_location='middle',
                                             name_gap=60,
                                             axislabel_opts=opts.LabelOpts(rotate=45)),
                    yaxis_opts=opts.AxisOpts(name=se),
                    legend_opts=opts.LegendOpts(is_show=False)
                )



        return bar


    def title(self):
        bar = (
            charts.Bar(init_opts=opts.InitOpts(width='90%', height='10%', theme=ThemeType.LIGHT)
                       )
                .set_global_opts(title_opts=opts.TitleOpts(title="未下推采购申请单/委外订单监控报表",
                                                           pos_left="center",
                                                           pos_top='20%',
                                                           title_textstyle_opts=opts.TextStyleOpts(font_size=30),
                                                           ),
                                 yaxis_opts=opts.AxisOpts(is_show=False),
                                 xaxis_opts=opts.AxisOpts(is_show=False)
                                 )
        )

        return bar

    def subtitle(self, title, subtitle,font_size):
        bar = (
            charts.Bar(init_opts=opts.InitOpts(width='90%', height='10%', theme=ThemeType.LIGHT)
                       )
                .set_global_opts(title_opts=opts.TitleOpts(title=title,
                                                           subtitle=subtitle,
                                                           pos_left="1%",
                                                           pos_top='12%',
                                                           item_gap=15,
                                                           title_textstyle_opts=opts.TextStyleOpts(
                                                               font_size=font_size,
                                                           color='#005487'),
                                                           subtitle_textstyle_opts=opts.TextStyleOpts(font_size=16,

                                                                                                      ),
                                                           ),
                                 yaxis_opts=opts.AxisOpts(is_show=False),
                                 xaxis_opts=opts.AxisOpts(is_show=False),
                                 )
        )

        return bar
















# PR_daily_unpushed_processed_chart()



