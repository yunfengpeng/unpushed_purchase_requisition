import getpass
import pandas as pd
from ldap3 import Server, Connection, ALL, NTLM, SAFE_SYNC, ALL_ATTRIBUTES
##
class get_staffs_group:
    def __init__(self, username, password):
        self.username = username
        self.password = password
        self.staffs_data = pd.DataFrame()
        self.get_staffs_data()
        #self.staffs_data.to_excel('./staffs_group_data.xlsx')


    def connect(self, username, password):
        server = Server(host='ad-dc01.flexiv.com', port=None, use_ssl=True, get_info=ALL)
        conn = Connection(server, user=f'flexiv.com\\{username}', password=password, client_strategy=SAFE_SYNC,
                authentication=NTLM)
        status, result, response, _ = conn.bind()
        assert status, result['description']
        return conn

    def fetch_all_users(self,conn):
        search_params = {
                        'search_base': 'OU=Users,OU=Flexiv,DC=flexiv,DC=com',
                        'search_filter': '(&(objectclass=user)(!(objectclass=computer))(name=*))',
                        'attributes': ALL_ATTRIBUTES,
        }
        users = []
        status, result, response, _ = conn.search(**search_params)
        if status:
            for entry in response:
                attrs = entry['attributes']
                users.append([
                             attrs.get('Description', '-- Empty --'),
                             attrs.get('CN', '-- Emtpy --'),
                             attrs.get('UserPrincipalName', '-- Empty --'),
                             attrs.get('whenCreated').strftime('%Y-%m-%d'),
                             attrs.get('Title', '-- Empty --'),
                             attrs.get('DistinguishedName', '-- Empty --'),
                             attrs.get('Manager', None),
                ])
        return users

    def get_manager(self,conn, manager):
        search_params = {
                         'search_base': manager,
                         'search_filter': '(&(objectclass=user)(!(objectclass=computer))(name=*))',
                         'attributes': ALL_ATTRIBUTES,
        }
        status, result, response, _ = conn.search(**search_params)
        if status:
            entry = response[0]
            attrs = entry['attributes']
            return attrs['UserPrincipalName']
        return '-- Empty --'

    def get_staffs_data(self):
        password = self.password
        username = self.username
        if not password:
            password = getpass.getpass()

        conn = self.connect(username, password)
        users = self.fetch_all_users(conn)

        for user in users:
            user[0] = user[0][0] if isinstance(user[0], list) else user[0]
            group = user[5].split(',')[1].split('=')[1]
            parent_group = user[5].split(',')[2].split('=')[1]
            Manager = self.get_manager(conn, user[6]) if user[6] else 'Empty'
            if user[6] == None:
                Manager_group = 'Empty'
            else:
                Manager_group = user[6].split(',')[1].split('=')[1]
            user.append(group)
            user.append(parent_group)
            user.append(Manager)
            user.append(Manager_group)


        conn.unbind()
        self.staffs_data = pd.DataFrame(users, columns=['Name_cn', 'Name_en', 'Email', 'Joined_at', 'Title', 'DN',
                                                   'Manager_info', 'Group', 'Parent_group','Manager', 'Manager_group'])

        user_group_df = self.staffs_data[['Email', 'Group']]
        user_group_tuple = user_group_df.apply(tuple,axis=1)
        user_group_tuple_list = list(user_group_tuple)

        parent_group_list = list(self.staffs_data['Parent_group'])
        manager_list = list(self.staffs_data['Manager'])
        if_root_group_list = []
        if_manager_list = []
        for user_group in user_group_tuple_list:

            if user_group[1] in parent_group_list:
                if_root_group = 0
            else:
                if_root_group = 1
            if_root_group_list.append(if_root_group)

            user_email = user_group[0]
            if user_email in manager_list:
                if_manager = 1
            else:
                if_manager = 0

            if_manager_list.append(if_manager)



        self.staffs_data['if_root_group'] = if_root_group_list
        self.staffs_data['if_manager'] = if_manager_list

        #print(self.staffs_data[self.staffs_data['DN'].str.contains('OU=Production Operation')])


# get_staffs_group()
