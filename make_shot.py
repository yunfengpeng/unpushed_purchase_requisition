
import time
import os.path

from selenium import webdriver
from pychromedriver import chromedriver_path

def webshot(url, data_dir,saveImgName):
    options = webdriver.ChromeOptions()
    options.add_experimental_option("excludeSwitches", ['enable-automation'])
    options.add_argument('--headless')
    options.add_argument('--disable-gpu')
    options.add_argument('--no-sandbox')
    driver = webdriver.Chrome(options=options,
                              executable_path=chromedriver_path,)
    driver.maximize_window()
    # 返回网页的高度的js代码
    js_height = "return document.body.clientHeight"
    #js_width = "return document.body.clientWidth"
    picname = saveImgName
    link = url
    pic_file_name = os.path.join(data_dir, picname)

    try:
        driver.get(link)
        k = 1
        height = driver.execute_script(js_height)
        while True:
            if k * 500 < height:
                js_move = "window.scrollTo(0,{})".format(k * 500)
                print(js_move)
                driver.execute_script(js_move)
                time.sleep(0.2)
                height = driver.execute_script(js_height)
                k += 1
            else:
                break
        #scroll_width = driver.execute_script('return document.body.parentNode.scrollWidth')
        scroll_width = 1800

        scroll_height = driver.execute_script('return document.body.parentNode.scrollHeight')

        driver.set_window_size(scroll_width, scroll_height)
        time.sleep(1)
        driver.get_screenshot_as_file(pic_file_name)

        print("Process {} get one pic !!!".format(os.getpid()))
        time.sleep(0.1)
    except Exception as e:
        print(picname, e)

    return  pic_file_name
