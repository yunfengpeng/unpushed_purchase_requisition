from KingdeeWebApi import Kingdee_Base_Web_Api
import requests
import pandas as pd
import json
import  os
##


class Get_Kingdee_Data(Kingdee_Base_Web_Api):
    def __init__(self):
        super().__init__()

        # if os.path.exists('.\数据分析') == False:
        #     os.makedirs('.\数据分析')
        # writer = pd.ExcelWriter(r'.\数据分析\生产环境采购总表.xlsx', engine='openpyxl')

        self.purchase_order = pd.DataFrame()
        self.Get_Purchase_Order()
        self.purchase_requisition_order = pd.DataFrame()
        self.Get_Purchase_Requisition_Order()
        self.sub_subreqorder_order = pd.DataFrame()
        self.Get_Sub_Subreqorder()



        # self.purchase_instock_order = pd.DataFrame()
        # self.Get_Purchase_InStock_Order()
        # self.receivebill_order = pd.DataFrame()
        # self.Get_ReceiveBill_Order()

        #
        # self.purchase_order.to_excel(writer,  sheet_name='采购订单')
        # self.purchase_requisition_order.to_excel(writer,  sheet_name='采购申请单')
        # self.sub_subreqorder_order.to_excel(writer, sheet_name='委外订单')



        # self.purchase_instock_order.to_excel(writer, sheet_name='采购入库单')
        # self.receivebill_order.to_excel(writer, sheet_name='收料通知单')

        #
        # writer.save()
        # writer.close()


    def Get_Sub_Subreqorder(self):
        Sub_Subreqorder_filiter = json.dumps({"FormId": "SUB_SUBREQORDER",
                                              "Limit": "0",
                                              "FieldKeys": "FID ,FBillNo ,FDocumentStatus ,FApproverId.fname ,"
                                                           "FApproveDate ,\
                                                        FModifierId ,FCreateDate ,FCreatorId.fname ,FModifyDate ,"
                                                           "FCancelDate ,\
                                              FCanceler.fname, FCancelStatus, FDescription, FSubOrgId.fname, "
                                                           "FOwnerTypeId, FOwnerId,\
                                              FPlannerID.fname, FDATE,FBillType.fname,FWorkGroupId, FIsRework, "
                                                           "FParentRowId,\
                                              FRowExpandType,FRowId,FMaterialId.fnumber,FProductType,FMaterialName,"
                                                           "FSpecification,\
                                              FUnitID.fnumber, FQty, FPlanStartDate,FPlanFinishDate, FBomId,"
                                                           "FSupplierId.fname,"
                                                           "FStatus,\
                                              FRequestOrgId, FRoutingId,FYieldRate, FStockInLimitH,FStockInLimitL,FStockID,\
                                              FStockLOCID,FAuxPropID,FMTONO,FProjectNo,FOperId,\
                                              FProcessId,FCostRate,FDescription1,FPlanConfirmDate,FConveyDate,FFinishDate,\
                                              FCloseDate,FCostDate,FStockInQty,FPurSelQty,FPurQty,FCreateType,FGroup,\
                                              FSrcBillId, FSrcBillEntrySeq,FSrcBillEntryId,FSaleOrderId,FSALEORDERNO,\
                                              FSaleOrderEntrySeq,FSaleOrderEntryId,FPurOrderId,FPurOrderNo,FPurOrderEntrySeq,\
                                              FBaseUnitId,FStockInOrgId,FPurOrderEntryId,FBaseUnitQty,FBaseStockInLimitH,\
                                              FBaseStockInLimitL,FBaseStockInQty,FBasePurSelQty,FSrcBillType,FSrcBillNo,\
                                              FLot,FPurorgId,FCopyEntryId,FBasePurQty,FBFLowId,FPriority,"
                                                           "FSettleOrgId.fname,\
                                              FInStockOwnerTypeId,FInStockOwnerId,FBaseYieldQty,FYieldQty,FSampleDamageQty,\
                                              FBaseSampleDamageQty,FStockReadyqty,FBaseStockReadyqty,FReqSrc,FBaseNoStockInQty,\
                                              FNoStockInQty,FIsSuspend,FBasePickMtlQty,FPickMtlQty,FISNEWLC,FPickMtrlStatus,\
                                              FSrcSplitBillNo,FSrcSplitSeq,FSrcSplitEntryId,FSrcSplitId,FFORCECLOSERID,\
                                              FCloseType,FSrcBomEntryId,FCloseReason,FConfirmId,FReleaseId,FFinishId, FIsMRP,\
                                              FTreeEntity_Link_FFlowId,FTreeEntity_Link_FFlowLineId,FTreeEntity_Link_FRuleId,\
                                              FTreeEntity_Link_FSTableId,FTreeEntity_Link_FSTableName,FTreeEntity_Link_FSBillId,\
                                              FTreeEntity_Link_FSId,FTreeEntity_Link_FBaseUnitQtyOld,FTreeEntity_Link_FBaseUnitQty,\
                                              FTreeEntity_FEntryId, F_FX_PURCHASEGROUP.fname, F_PAEZ_Base.fname,"
                                              "F_PENQ_RDPROJECT,F_PENQ_REQUESTER,F_PENQ_PortalTitle,"
                                                           "F_PAEZ_Assistant.fnumber"
                                                           })
        post_data = {"data": Sub_Subreqorder_filiter}
        response = requests.post(url=self.query_url, data=post_data, cookies=self.login(), verify=False)


        self.sub_subreqorder_order = pd.DataFrame(json.loads(response.text)
                                                  ,columns=['实体主键','单据编号','单据状态','审核人',
                                             'sub审核日期','修改人','创建日期','创建人','修改日期','作废日期','作废人','作废状态','备注',
                                             '委外组织','货主类型','货主','计划员','单据日期','单据类型','计划组','是否返工',
                                             '父级行主键','行展开类型','行标识','物料编码','产品类型','物料名称','规格型号','单位','数量',
                                             '计划开工时间','计划完工时间','BOM版本','供应商','业务状态','需求组织','工艺路线','成品率(%)',
                                             '入库上限','入库下限','仓库','仓位','辅助属性','计划跟踪号','项目编号',
                                             '产出工序','产出作业编码','成本权重','备注','计划确认日期','下达日期','完工日期','结案日期','结算日期',
                                             '入库数量','采购选单数量','采购执行数量','生成方式','组号','来源单据ID','源单分录行号',
                                             '来源单据分录内码','销售订单ID','需求单据','需求单据行号','销售订单分录内码','采购订单ID',
                                             '采购订单','采购订单行号','基本单位','入库组织','采购订单分录内码','基本单位数量','基本单位入库上限',
                                             '基本单位入库下限','基本单位入库数量','基本单位采购选单数量','源单类型','源单编号','批号',
                                             '采购组织','联副产品分录内码','基本单位采购执行数量','业务流程','需求优先级','结算组织',
                                             '入库货主类型','入库货主','基本单位成品数量','成品数量','样本破坏数量','基本单位样本破坏数量','已备料数量',
                                             '基本单位已备料数量','需求来源','基本单位未入库数量','未入库数量','挂起状态','基本单位领料套数',
                                             '领料套数','是否手工新增联副产品','领料状态','源拆分订单编号','源拆分订单行号','源拆分订单分录内码',
                                             '源拆分生产订单内码','结案人','结案类型','上级BOM展开分录内码','强制结案原因','计划确认人',
                                             '下达人','完工人','已计划运算','业务流程图','推进路线','转换规则','源单表内码',
                                            '源单表','源单内码','源单分录内码','原始携带量','修改携带量','明细主键', '采购组_head',
                                            '采购员_head', '研发项目','需求人','PortalTitle','用途']
                                                  )

    def Get_Purchase_Requisition_Order(self):
        purchase_Requisition_filiter = json.dumps({"FormId": "PUR_Requisition",
                                                   "Limit" : "0",
                                                   "FieldKeys": "FID ,FBillNo ,FDocumentStatus ,FBillTypeID.fname ,\
                                             FApplicationOrgId.fname,FApplicationDeptId.fname ,FApplicantId.fname ,"
                                             "FCurrencyId ,FTotalAmount ,FCancellerId ,FCloseStatus ,FCloserId.fname ,"
                                             "FCreatorId ,FCloseDate,FCancelDate ,\
                                             FApproverId ,FModifierId.fname ,FCreateDate ,FCancelStatus ,FModifyDate ,"
                                                                "FApproveDate ,\
                                             FExchangeTypeId ,FNote ,FIsConvert ,FRequestType ,FApplicationDate,FIsMergeCancel ,\
                                             FSrcType ,FPublishStatus ,FISPRICEEXCLUDETAX ,FACCTYPE ,FMANUALCLOSE ,F_PAEZ_Text ,\
                                             F_PAEZ_Combo ,F_PAEZ_Base.fname ,F_FX_PURCHASEGROUP.fname ,F_FX_PORTALNO ,"
                                                                "F_FX_PORTALRECIEVER ,\
                                             F_FX_PlanStartMonth ,F_FX_Memo ,FEntity_FEntryID ,FMaterialName ,\
                                             FMaterialModel ,FMaterialType ,FReqQty ,FApproveQty ,FSuggestSupplierId ,\
                                             FLeadTime ,FReqAmount ,FPurchaseOrgId.fname ,FPurchaseGroupId.fname ,"
                                                                "FReceiveOrgId ,\
                                             FStockId ,FOrderQty ,FRemainQty ,FContractNo ,FReqTraceNo ,FMRPTerminateStatus ,\
                                             FTerminaterId ,FTerminateDate ,FMRPCloseStatus ,FPlanConfirm ,"
                                                                "FSupplierId.fname ,\
                                             FMaterialId.fnumber ,FPriceUnitQty ,FPurchaseDeptId ,FReceiveAddress ,"
                                                                "FEntryNote ,\
                                             FOrderJNBaseQty ,FOrderBaseQty ,FBaseUnitId ,FPriceUnitId ,"
                                                                "FUnitId.fname ,FOrderJoinQty ,\
                                             FBaseUnitQty ,FAuxpropId ,FBOMNoId ,FSrcBillTypeId ,FSrcBillNo ,FMaterialDesc ,\
                                             FSuggestPurDate ,FArrivalDate ,FBaseStockQty ,FBaseReceiveQty ,FStockQty ,\
                                             FReceiveQty ,FBFLowId ,FProviderId ,FIsSplitCancel ,FMtoNo ,FRequireOrgId ,\
                                             FPurchaserId.fname ,FBaseReqQty ,FReceiveDeptId ,FRequireDeptId ,"
                                                                "FChargeProjectID ,\
                                             FEvaluatePrice ,FQueryPriceId ,FRowPublishStatus ,FSalUnitID ,FSalQty ,FSalBaseQty ,\
                                             FTAXPRICE ,FHASPUBLISH ,FREQSTOCKUNITID ,FREQSTOCKQTY ,FREQSTOCKBASEQTY ,FORDERSTOCKJNBASEQTY ,\
                                             FPRICEBASEQTY ,FSRCBIZUNITID ,FPURBASENUM ,FSTOCKBASEDEN ,FIsVmiBusiness ,\
                                             FTAXRATE ,FAmount ,FPriceListEntry ,FInquiryJoinQty ,FInquiryJoinBaseQty ,\
                                             FDownPrice ,FUpPrice ,FDEMANDTYPE ,FDEMANDBILLNO ,FDEMANDBILLENTRYSEQ ,FDEMANDBILLENTRYID ,\
                                             FRowType ,FParentMaterialId ,FParentBomId ,FParentRowId ,FSrcReqSplitEntryId ,\
                                             FSrcReqMergeEntryIds ,FPRILSTENTRYID ,FAssortBillNo ,"
                                                                "F_PAEZ_Assistant.fnumber ,\
                                             F_PAEZ_BaseProperty ,F_PAEZ_BaseProperty1 ,F_FX_OutStockApplyQTY ,F_PAEZ_BaseProperty2 ,\
                                             F_FX_NetPRQty ,F_FX_ReplyDate ,F_FX_ZZS ,FMobBillHead_FEntryId ,FExtendField ,\
                                             FOrderName ,FIsMobBill ,FMobIsPending ,FEntity_Link_FLinkId ,FEntity_Link_FFlowId ,\
                                             FEntity_Link_FFlowLineId ,FEntity_Link_FRuleId ,FEntity_Link_FSTableId ,FEntity_Link_FSTableName ,\
                                             FEntity_Link_FSBillId ,FEntity_Link_FSId ,FEntity_Link_FBaseUnitQtyOld ,FEntity_Link_FBaseUnitQty ,\
                                             FEntity_Link_FSalBaseQtyOld ,FEntity_Link_FSalBaseQty ,FEntity_Link_FREQSTOCKBASEQTYOld ,\
                                             FEntity_Link_FREQSTOCKBASEQTY ,FEntity_Link_FLnkTrackerId ,FEntity_Link_FLnkSState ,\
                                             FEntity_Link_FLnkAmount ,FEntity_Link_FLINKTrackerId ,FEntity_Link_FLINKSState ,\
                                             FEntity_Link_FLINKAmount,'F_PENQ_REQUESTER','F_PENQ_PortalTitle',"
                                             "F_PENQ_RDPROJECT  "})
        post_data = {"data": purchase_Requisition_filiter}
        response = requests.post(url=self.query_url, data=post_data, cookies=self.login(),verify=False)
        self.purchase_requisition_order = pd.DataFrame(json.loads(response.text),
                                                       columns=['维度主键','单据编号','数据状态','单据类型','申请组织','申请部门',
                                            '申请人','币别','含税金额合计','作废人','关闭状态','关闭人','创建人','关闭日期',
                                            '作废日期','审核人','最后修改人','创建日期','作废状态','最后修改日期','pr审核日期','汇率类型',
                                            '备注','是否是单据转换','申请类型','申请日期','合并作废','来源类型','供应商协同发布状态(6.1废弃)',
                                            '价外税','验收方式','手工关闭','收货人','收货地址','采购员_head','采购组_head','Portal项目号',
                                            'Portal收货人','建议下单年/月','备忘录','明细主键','物料名称','规格型号','物料类别',
                                            '申请数量','批准数量','建议供应商','提前期','含税金额','采购组织','采购组','收料组织',
                                            '仓库','订单数量','剩余数量','合同号','需求跟踪号','业务终止','终止人','终止日期',
                                            '业务关闭','计划确认','指定供应商(6.0作废)','物料编码','计价数量','采购部门','交货地址',
                                            '备注','订单关联数量（基本单位）','订单数量（基本单位）','基本单位','计价单位','申请单位',
                                            '订单关联数量','批准数量(基本单位)','辅助属性','BOM版本','源单类型','源单编号','物料说明',
                                            '建议采购日期','到货日期','入库数量(基本单位)','收料数量(基本单位)','入库数量','收料数量',
                                            '业务流程','供货地点','拆分作废','计划跟踪号','需求组织','采购员','申请数量(基本单位)',
                                            '收料部门','需求部门','费用项目','单价','1688询价单ID','供应商协同行发布状态(6.1废弃)',
                                            '销售单位','销售数量','销售基本数量','含税单价','1688发布状态','库存单位','库存单位数量',
                                            '库存基本数量','订单库存关联数量(基本单位)','计价基本数量','携带的主业务单位','采购基本分子',
                                            '库存基本分母','VMI业务','税率%','金额','价目表','关联询价数量','关联询价数量（基本单位）',
                                            '价格下限','价格上限','需求来源','需求单据编号','需求单据行号','需求单据分录内码','产品类型',
                                            '父项产品','父项BOM版本','父产品行主键','申请单拆单前分录内码','申请单合并前分录内码',
                                            '价目表分录id','配套单据编号','用途','Sourcing','制造商','累计出库申请基本数量',
                                            '物料备注','计划净需求','供应商回复交期','制造商','实体主键','扩展字段','订单名称',
                                            '移动单据','是否挂单','实体主键','业务流程图','推进路线','转换规则','源单表内码',
                                            '源单表','源单内码','源单分录内码','原始携带量','修改携带量','原始携带量','修改携带量',
                                            '原始携带量','修改携带量','迁移图','上游状态','数量FLnk','迁移图','上游状态','数量FLINK'
                                             ,'需求人','PortalTitle','研发项目'
                                            ]
                                                       )

        self.purchase_requisition_order = self.purchase_requisition_order[
                                                self.purchase_requisition_order['采购组织']=='佛山非夕机器人科技有限公司'
                                          ]


    def Get_Purchase_Order(self):
        purchase_order_filter = json.dumps({"FormId": "PUR_PurchaseOrder",
                                            "Limit": "0",
                                            "FieldKeys": "FBillTypeID.fname , FBillNo , Fdate , FSrcBillTypeId , FSrcBillNo ,\
                                                          FSupplierId.fname ,FSupplierId.FShortName, FDocumentStatus, \
                                                          FPurchaseOrgId.fname , FPurchaserGroupId.fname , FPurchaserId.fname , FCloseStatus ,\
                                                          FMaterialType, FMaterialId.fnumber, FMaterialName , FModel ,FUnitId.fname , FQty,\
                                                          FDeliveryDate , FGiveAway , FMRPCloseStatus , FMRPTerminateStatus ,\
                                                          FReceiveQty , FRemainReceiveQty , FRemainStockINQty ,	F_PAEZ_Date ,\
                                                          F_PAEZ_Assistant.fnumber , Fid , FPOOrderEntry_FEntryID , \
                                                          FPOOrderEntry_Link_FSBillId , FPOOrderEntry_Link_FSId ,\
                                                          FCreateDate, FApproveDate "})

        post_data = {"data": purchase_order_filter}
        response = requests.post(url=self.query_url, data=post_data, cookies=self.login(), verify=False)
        self.purchase_order = pd.DataFrame(json.loads(response.text), columns=['单据类型02', '单据编号02', '采购日期', '源单类型02',
                                                                               '源单编号02', '供应商02', '供应商简称02', '单据状态1',
                                                                               '采购组织','采购组02', '采购员02', '关闭状态', '物料类别02',
                                                                               '物料编码02', '物料名称02', '规格型号', '采购单位',
                                                                               '采购数量', '交货日期', '是否赠品', '业务关闭',
                                                                               '业务终止', '累计收料数量', '剩余收料数量',
                                                                               '剩余入库数量', '供应商回复交期', '用途', '维度主键02',
                                                                               '明细表主键02', '源单内码02', '源单分录内码02',
                                                                               '创建日期', 'PO审核日期'])
        self.purchase_order['源单内码02'] = pd.to_numeric(self.purchase_order['源单内码02'], errors='coerce')
        self.purchase_order['源单分录内码02'] = pd.to_numeric(self.purchase_order['源单分录内码02'], errors='coerce')
        self.purchase_order['物料类别'] = self.purchase_order['物料编码02'].str[0:3]
        self.purchase_order = self.purchase_order[self.purchase_order['采购组织'] == '佛山非夕机器人科技有限公司']
