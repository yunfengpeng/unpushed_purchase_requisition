
import requests
import json
import pandas as pd
from urllib3.exceptions import InsecureRequestWarning
import urllib3
from decouple import config
##
if config('DEBUG'):
    urllib3.disable_warnings(InsecureRequestWarning)

class Kingdee_Base_Web_Api():
    def __init__(self):
     self.cookies=None
     self.base_url = config('KD_BASE_URL')   #https://flexiv.ik3cloud.com/K3cloud #http://180.169.17.230:880/k3cloud" #https://flexiv.ik3cloud.com/K3cloud
     self.login_url = self.base_url + "/Kingdee.BOS.WebApi.ServicesStub.AuthService.ValidateUser.common.kdsvc"
     self.login_data = {
         "acctid": config('KD_ACCOUNT_ID'),
         "username": config('KD_USERNAME'),
         "password": config('KD_PASSWORD'),
         "lcid": config('KD_LCID', cast=int),
     }
     #20210329181910599  #"5feab7b4f8ca23" "20210329181910599"

     self.submit_url = self.base_url + "/Kingdee.BOS.WebApi.ServicesStub.DynamicFormService.Submit.common.kdsvc"
     self.push_url = self.base_url + "/Kingdee.BOS.WebApi.ServicesStub.DynamicFormService.Push.common.kdsvc"
     self.query_url = self.base_url + "/Kingdee.BOS.WebApi.ServicesStub.DynamicFormService.ExecuteBillQuery.common.kdsvc"
     self.save_url = self.base_url + "/Kingdee.BOS.WebApi.ServicesStub.DynamicFormService.Save.common.kdsvc"
     self.batch_save = "/Kingdee.BOS.WebApi.ServicesStub.DynamicFormService.BatchSave.common.kdsvc"
     self.login()

    def login(self):
        login_response = requests.post(self.login_url, self.login_data,verify=False)
        login_text = json.loads(login_response.text)
        self.cookies = login_response.cookies    #将首次登陆信息保存到cookies中
        if login_text['LoginResultType'] == 1:
            login_result ='登陆成功'
        else:
            login_result ='登陆失败'
        return login_response.cookies

Kingdee_Base_Web_Api()
