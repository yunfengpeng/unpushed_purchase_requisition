
from pyecharts import charts ##
from pyecharts import options as opts
from pyecharts.globals import ThemeType
from unpushed_chart import chart
import data_clean
CHART = chart()

def page_layout(inputs, html_name):
    """获取清洗后的数据"""

    pr_daily_total_detail = inputs['pr_daily_total_detail']
    sub_daily_total_detail = inputs['sub_daily_total_detail']
    pr_unpushed_detail_data = inputs['pr_unpushed_detail_data']
    sub_unpushed_detail_data = inputs['sub_unpushed_detail_data']
    # pr_se_list = list(pr_unpushed_detail_data['Sourcing'].drop_duplicates())

    
    page = charts.Page(page_title='未下推采购申请单/委外订单监控报表', layout=charts.Page.SimplePageLayout, interval=0)
    grid_title = charts.Grid(init_opts=opts.InitOpts(width='100%', height='150px', theme=ThemeType.LIGHT))
    grid_title.add(CHART.title(), grid_opts=opts.GridOpts(pos_bottom="30%", pos_top="30%", pos_right="25%"))
    page.add(grid_title)

    """                                         PR未下推分析                           """
    """一级标题"""
    flag= 'PR'
    title = 'PR未下推分析'
    subtitle =''
    grid_pr_title = charts.Grid(init_opts=opts.InitOpts(width='100%',height='50px',theme=ThemeType.LIGHT))
    grid_pr_title.add(CHART.subtitle(title,subtitle,26),
                          grid_opts=opts.GridOpts(pos_bottom="20%")
                          )
    page.add(grid_pr_title)

    """添加PR部门维度剩余未下推趋势图和新增行数和已下推行数趋势图"""
    grid_pr_dep_unpush = charts.Grid(init_opts=opts.InitOpts(width='100%', height='650px', theme=ThemeType.LIGHT)
                                         )
    grid_pr_dep_unpush.add(CHART.dep_remaining_unpushed_chart(pr_unpushed_detail_data, flag),
                           grid_opts=opts.GridOpts(pos_top="15%",
                                                   pos_bottom='55%',
                                                   pos_left='5%',
                                                   pos_right='5%',
                                                   is_contain_label=True)
                              )
    grid_pr_dep_unpush.add(CHART.dep_daily_unpushed_processed_chart(pr_daily_total_detail, flag), ###改
                                           grid_opts=opts.GridOpts(pos_top="60%",
                                                                   pos_bottom='10%',
                                                                   pos_left='5%',
                                                                   pos_right='5%',
                                                                   is_contain_label=True
                                                                   )
                              )
    page.add(grid_pr_dep_unpush,
                 )


    """添加各SE的剩余未下推趋势图"""

    se_pr_list = list(pr_unpushed_detail_data['Sourcing'].drop_duplicates())
    height = 250 * (len(se_pr_list) + 1)
    grid_pr_unpushed = charts.Grid(init_opts=opts.InitOpts(width='50%',
                                                               height=str(height)+'px',
                                                               theme=ThemeType.LIGHT,
                                                               )
                                       )
    """添加各SE建议采购日期分布图"""
    grid_pr_date = charts.Grid(init_opts=opts.InitOpts(width='50%',
                                                           height=str(height)+'px',
                                                           theme=ThemeType.LIGHT,
                                                           )
                                   )

    """计算图高度比率"""
    top_rate_pr_list = [250 * (i + 1) for i in range(len(se_pr_list))]
    diff = 250
    bottom_rate_pr_list = [height - i - diff for i in top_rate_pr_list]

    for i, se in enumerate(se_pr_list):
        grid_pr_unpushed.add(CHART.se_remaining_unpushed_chart(se,
                                                               pr_unpushed_detail_data,
                                                               pr_daily_total_detail,
                                                               flag,
                                                               i),
                             grid_opts=opts.GridOpts(
                                                     pos_top=str(top_rate_pr_list[i] - 100) + 'px',
                                                     pos_bottom=str(bottom_rate_pr_list[i] + 170) + 'px',
                                                     is_contain_label=True
                                                    ),
                            )

        grid_pr_date.add(CHART.suggested_purchase_date_chart(se,
                                                             pr_unpushed_detail_data,
                                                             flag,
                                                             i),
                          grid_opts=opts.GridOpts( pos_top=str(top_rate_pr_list[i] - 100) + 'px',
                                                   pos_bottom=str(bottom_rate_pr_list[i] + 170) + 'px',
                                                   is_contain_label=True
                                                 )
                         )

    page.add(grid_pr_unpushed)
    page.add(grid_pr_date)



    """                                委外订单                   """
    """添加SUB部门维度剩余未下推趋势图和新增行数和已下推行数趋势图"""
    title = '委外未下推分析'
    subtitle = ''
    grid_sub_title = charts.Grid(init_opts=opts.InitOpts(width='100%', height='50px', theme=ThemeType.LIGHT))
    grid_sub_title.add(CHART.subtitle(title, subtitle,26),
                           grid_opts=opts.GridOpts(pos_bottom="20%"))
    page.add(grid_sub_title)


    flag = '委外' #sub
    grid_sub_dep_unpush = charts.Grid(init_opts=opts.InitOpts(width='100%', height='650px', theme=ThemeType.LIGHT)
                                         )

    grid_sub_dep_unpush.add(CHART.dep_remaining_unpushed_chart(sub_unpushed_detail_data,flag),
                               grid_opts=opts.GridOpts(pos_top="15%",
                                                       pos_bottom='55%',

                                                       pos_left='5%',
                                                       pos_right='5%',
                                                       is_contain_label=True)
                               )
    grid_sub_dep_unpush.add(CHART.dep_daily_unpushed_processed_chart(sub_daily_total_detail, flag),  ###改
                               grid_opts=opts.GridOpts(pos_top="60%",
                                                       pos_bottom='10%',
                                                       pos_left='5%',
                                                       pos_right='5%',
                                                       is_contain_label=True
                                                       )
                               )
    page.add(grid_sub_dep_unpush,
                 )


    """添加各SE的剩余未下推趋势图"""
    se_sub_list = list(sub_unpushed_detail_data['Sourcing'].drop_duplicates())
    height = 250 * (len(se_sub_list) + 1)
    grid_se_sub = charts.Grid(init_opts=opts.InitOpts(width='50%', height=str(height)+ 'px', theme=ThemeType.LIGHT,
                                                     )
                                  )
    grid_sub_date = charts.Grid(init_opts=opts.InitOpts(width='50%', height=str(height) + 'px',theme=ThemeType.LIGHT,
                                                           )
                                  )

    top_rate_sub_list = [250 * (i + 1) for i in range(len(se_sub_list))]
    diff = 250
    bottom_rate_sub_list = [height - i - diff for i in top_rate_sub_list]


    se_sub_list=['林蔚','刘铭兰','彭子娟']
    for i, se in enumerate(se_sub_list):
        grid_se_sub.add(CHART.se_remaining_unpushed_chart(se, sub_unpushed_detail_data,
                                                             sub_daily_total_detail,
                                                             flag,
                                                             i),
                            grid_opts=opts.GridOpts(
                                                    pos_top=str(top_rate_sub_list[i]-100) + 'px',
                                                    pos_bottom=str(bottom_rate_sub_list[i]+170) + 'px',
                                                    is_contain_label=True
                                                    ),
                           )

        grid_sub_date.add(CHART.suggested_purchase_date_chart(se,
                                                                 sub_unpushed_detail_data,
                                                                 flag,
                                                                 i),
                              grid_opts=opts.GridOpts(
                                                      pos_top=str(top_rate_sub_list[i]-100) + 'px',
                                                      pos_bottom=str(bottom_rate_sub_list[i]+170) + 'px',
                                                      is_contain_label=True
                                                        )
                                 )

    page.add(grid_se_sub)
    page.add(grid_sub_date)


    page.render(html_name)











